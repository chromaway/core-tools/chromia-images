# Chromia Docker images

When changing a dockerfile, bump the version in the automation file and run this stage manually when MR is merged

## How to pick the correct image digest

**Option 1:** Copy the _Index digest_ directly from the multi-platform image page at [Docker Hub](https://hub.docker.com/layers/library/postgres/14.9-alpine3.18/images/sha256-6e3e00a57dc1313f19ecfa425d7437cb529e1044b090b9c889d89c873f7b3e4d).

**Option 2:** Retrieve the digest using `docker pull`:
```
$ docker pull postgres:14.9-alpine3.18 | grep Digest
Digest: sha256:874f566dd512d79cf74f59754833e869ae76ece96716d153b0fa3e64aec88d92
```

**Option 3:** Retrieve the digest using the `manifest-tool` utility:
```
$ docker run mplatform/manifest-tool inspect postgres:14.9-alpine3.18 | grep Digest | head -n1
Digest: sha256:874f566dd512d79cf74f59754833e869ae76ece96716d153b0fa3e64aec88d92
```

The final image to use in the `Dockerfile` would be

```
FROM postgres:14.9-alpine3.18@sha256:874f566dd512d79cf74f59754833e869ae76ece96716d153b0fa3e64aec88d92
``` 