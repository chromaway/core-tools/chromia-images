# Migration from postgres 14 to postgres 16

This guide will show you how to migrate a nodes container databases from postgres 14 to postgres 16.

## Steps

Follow these steps to migrate your node. Some steps are optional but recommended.

### 1. Stop the node

Stop the node and make sure no Postchain sub container is running before starting the migration.

```bash
docker ps -f label=postchain-master-pubkey
```

If the node container is stopped, but there still are sub containers running, you can stop them with:

```bash
docker stop $(docker ps -f label=postchain-master-pubkey -q)
```

### 2. Preparation

Configure the migration with the following environment variables:

```bash
# Same as your `container.host-mount-dir` in your node config
CONTAINER_HOST_MOUNT_DIR=/data/chromia/subnodes

# Same as your `subnode.user` in your node configu
SUBNODE_USER=1000:1000

# Set the node url to the system node - this will be used to find and exclude container with custom postgres images
NODE_URL=https://system.chromaway.com:7740
```

The migration will require a temporary directory for Postgres socket files and log file, and also a backup location (unless skipped). The migration will use the follow default paths for the migration in the container:

```bash
mkdir -p /tmp/{pgdatamigration,backup}

# Make sure the directories are owned by the sub node user
chown $SUBNODE_USER /tmp/{pgdatamigration,backup}
```

### 3. Disable ext4 file quotas on the host (optional)

This step is optional but highly recommended. You can skip it if you know none of the containers use more than 50% of its disk quota. If unsure, you should disable quotas.

```bash
# Unmount the filesystem which currently do use quotas
umount $CONTAINER_HOST_MOUNT_DIR

# Mount the filesystem without quotes, replace with correct device
mount /dev/vdb1 $CONTAINER_HOST_MOUNT_DIR
```

### 4. Make backups and test the migration (optional)

Start the migration tool to make backups and test the migration.
Make sure your backup directory is empty and has enough free space to host all container data.

```bash
docker run --rm -it \
  --mount type=bind,source=/etc/passwd,target=/etc/passwd \
  --mount type=bind,source=/etc/group,target=/etc/group \
  --mount type=bind,source=$CONTAINER_HOST_MOUNT_DIR,target=/subnodes/ \
  --mount type=bind,source=/tmp/pgdatamigration,target=/tmp/pgdatamigration \
  --mount type=bind,source=/tmp/backup,target=/tmp/backup \
  -u $SUBNODE_USER -e NODE_URL=$NODE_URL \
  registry.gitlab.com/chromaway/core-tools/chromia-images/postgres14-16-migration:1.0.0 -b /tmp/backup
```

If successful, you should see the following output in the end:

```bash
Migration of X containers completed successfully.
```

This will create a backup of each container database in `/tmp/backup` and also try to migrate the database to the new version.

You can skip backup by omitting the `-b <directory>` option.

### 5. Run the migration

This will run the migration again and also replace the old database with the new one.

```bash
docker run --rm -it \
  --mount type=bind,source=/etc/passwd,target=/etc/passwd \
  --mount type=bind,source=/etc/group,target=/etc/group \
  --mount type=bind,source=$CONTAINER_HOST_MOUNT_DIR,target=/subnodes/ \
  --mount type=bind,source=/tmp/pgdatamigration,target=/tmp/pgdatamigration \
  --mount type=bind,source=/tmp/backup,target=/tmp/backup \
  -u $SUBNODE_USER -e NODE_URL=$NODE_URL \
  registry.gitlab.com/chromaway/core-tools/chromia-images/postgres14-16-migration:1.0.0 -r
```

### 6. Enable ext4 file quotas on the host

```bash
# Unmount the filesystem which currently do not use quotas
umount $CONTAINER_HOST_MOUNT_DIR

# Mount the filesystem based on the current /etc/fstab
mount -a
```

### 7. Update the sub node image

In your node configuration, update the `container.docker-image` value and set it to the new version:

```
container.docker-image=registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-subnode: # latest version or the one suggested by Chromaway
```

### 8. Start the node again

Now start the node and make sure it is healthy.

### 9. Cleanup

The migration tool might have identified some containers that can be removed. Feel free to remove them to free up some disk space:

```bash
# List containers that can be removed
grep "is not found" /tmp/pgdatamigration/migration.log                            
   - Container directory 123 is not found in directory-chain - SKIPPED MIGRATION
   - Container directory 456 is not found in directory-chain - SKIPPED MIGRATION
   - Container directory 789 is not found in directory-chain - SKIPPED MIGRATION
...

# Remove the container directories, use your path from `container.host-mount-dir`
rm -rf /data/chromia/subnodes/123/
rm -rf /data/chromia/subnodes/456/
rm -rf /data/chromia/subnodes/789/
...
```

If everything went well, you can remove the temporary directory and the backups.

```bash
rm -rf /tmp/pgdatamigration
rm -rf /tmp/backup
```
 

## Troubleshooting

### Migration fails, and the log says the database was not cleanly shutdown

This happens if the container Postgresql instance is not shut down properly.

There are two ways to fix this:

#### Option 1: Cleanup database with tool

Use the tool to start a Postgres instance and run the `vacuumdb` command on the database.

```bash
docker run --rm -it \
  --mount type=bind,source=/etc/passwd,target=/etc/passwd \
  --mount type=bind,source=/etc/group,target=/etc/group \
  --mount type=bind,source=$CONTAINER_HOST_MOUNT_DIR,target=/subnodes/ \
  --mount type=bind,source=/tmp/pgdatamigration,target=/tmp/pgdatamigration \
  --mount type=bind,source=/tmp/backup,target=/tmp/backup \
  -u $SUBNODE_USER -e NODE_URL=$NODE_URL \
  registry.gitlab.com/chromaway/core-tools/chromia-images/postgres14-16-migration:1.0.0 --vacuum <container-name>
```

#### Option 2: Restart the node

Have the node restart the containers.

1. Start the node again (this is fine, but containers migrated to v16 won't start)
2. Give it a minute to run
3. Stop the node
4. Run the migration again (already migrated data will be skipped)

