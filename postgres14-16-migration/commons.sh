#!/usr/bin/env bash

set -euo pipefail

echo_log() {
  verbose=true verbose_output echo "$@"
}

verbose_output() {
  if [ "$verbose" = true ]; then
    "$@" 2>&1 | tee -a "$MIGRATION_LOG"
  else
    "$@" >> "$MIGRATION_LOG" 2>&1
  fi
}

function pre_checks() {
  if [ "$(id -u)" == "0" ]; then
    echo "Do not run as root"
    exit 1
  fi

  if [ -z "$NODE_URL" ]; then
    echo "NODE_URL must be set"
    exit 1
  fi

  if [ "$(curl  -s -o /dev/null --write-out "%{http_code}" $NODE_URL)" != "200" ]; then
    echo "Node is not available at $NODE_URL"
    exit 1
  fi

  if [ ! -d "$SUBNODES_DIR" ]; then
    echo "Subnode data path is not correct: $SUBNODES_DIR"
    exit 1
  fi

  if [ ! -d "$WORKING_DIR" ]; then
    echo "Incorrect working directory: $WORKING_DIR"
    exit 1
  fi

  if [ "$CONTAINER_COUNT" -le 0 ]; then
    echo "No containers found in $SUBNODES_DIR"
    exit 1
  fi

  if [ -n "$backup_path" ]; then
    if [ ! -d "$backup_path" ]; then
      echo "Backup directory does not exist: $backup_path"
      exit
    fi

    test_file="$backup_path/testfile"
    if touch "$test_file" &>/dev/null; then
        rm -f "$test_file"
    else
      echo "Backup directory ($backup_path) is not writable by this user ($MY_UID)"
      exit 1
    fi
  fi
}
