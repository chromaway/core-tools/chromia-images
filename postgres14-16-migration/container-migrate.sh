#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

source "$SCRIPT_DIR/commons.sh"

container_path="$1"

function migrate_container {
  container_path="$1"
  container_path_name="$(basename "$container_path")"
  pgdata_path="$container_path/pgdata/"
  new_pgdata_path="$container_path/pgdata_v$PG_DESTINATION_VERSION"
  pg_version_path="$pgdata_path/PG_VERSION"
  container_name="$(echo $container_path_name | grep -Eo "[0-9A-F]{1,}-[^-]{1,}-[0-9]{1,}" | sed 's/^[^-]*-//g' | sed 's/-[^-]*$//g')"

  if [ ! -d "$pgdata_path" ] || [ ! -f "$pg_version_path" ]; then
    echo_log "Unexpected container data structure:"
    ls "$container_path"
    exit 1
  fi

  pg_version="$(cat "$pg_version_path")"
  if [ "$pg_version" == "$PG_DESTINATION_VERSION" ]; then
    echo_log "Container is already migrated to v$PG_DESTINATION_VERSION, skipping."
  elif [ "$pg_version" != "$PG_SOURCE_VERSION" ]; then
    echo_log "Unexpected postgres version in $pg_version_path, expected $PG_SOURCE_VERSION but got $pg_version"
    exit 1
  else

    if [ "$(find "$container_path" -exec stat -c "%u" {} \; | uniq)" != "$MY_UID" ]; then
      echo_log "Container files not fully owned by user $MY_UID"
      exit 1
    fi

    if [ "$(find "$container_path" -exec stat -c "%g" {} \; | uniq)" != "$MY_GID" ]; then
      echo_log "Container files not fully owned by group $MY_GID"
      exit 1
    fi

    if [ -e "$new_pgdata_path" ]; then
      echo_log "Aborting. Temporary migration directory already exists: $new_pgdata_path"
      echo_log "Did a previous migration fail? Investigate and manually remove the directory and run again"
      exit 1
    fi

    if [ "$skip_dc_verification" == false ]; then
      echo_log " - Verifying container $container_name in directory-chain"
      container_image_result="$(curl -s "$NODE_URL/query/$DC_BRID/?type=nm_get_container_image&name=$container_name")"
      if [ "$(echo "$container_image_result" | jq -r '.digest')" != "null" ]; then
        echo_log "   - Container is using extension image"
      elif echo "$container_image_result" | grep -q "not found"; then
        echo_log "   - Container directory $container_path_name is not found in directory-chain - SKIPPED MIGRATION"
        return
      fi
    else
      echo_log " - Verifying container $container_name in directory-chain (SKIPPED)"
    fi

    if [ -f "$pgdata_path/postmaster.pid" ]; then
      echo_log "Pid file $pgdata_path/postmaster.pid found - is postgres gracefully shutdown?"
      echo_log "If not, try to run the migration tool again with \"--vacuum $container_name\" to start and stop postgres gracefully"
      exit 1
    fi

    if [ -z "$backup_path" ]; then
      echo_log " - Backing up original data (SKIPPED)"
    else
      echo_log " - Backing up original data"

      container_backup_path="$backup_path/$container_path_name/"
      if [ -e "$container_backup_path" ]; then
        echo_log "Aborting. Container backup path already exists: $container_backup_path"
        exit 1
      fi

      mkdir -p "$container_backup_path"

      echo_log "   - Copying $container_path to $backup_path"
      cp -a "$container_path/" "$backup_path"
    fi

    echo_log " - Initializing new database in $new_pgdata_path"
    mkdir -p "$new_pgdata_path"
    verbose_output initdb --username=postgres -D "$new_pgdata_path"
    touch "$new_pgdata_path/.db-initialized"

    echo_log " - Migrate data from $pgdata_path to $new_pgdata_path"
    verbose_output pg_upgrade --old-datadir "$pgdata_path" --new-datadir "$new_pgdata_path" --old-bindir "$PG_BIN_14" --new-bindir /usr/local/bin/ -U postgres

    echo_log " - Verifying and cleaning up data"
    pg_ctl -D "$new_pgdata_path" start >> "$MIGRATION_LOG" 2>&1
    verbose_output /usr/local/bin/vacuumdb -U postgres --all --analyze-in-stages
    verbose_output pg_ctl -D "$new_pgdata_path" stop

    echo_log "   - Old size: $(du -hs "$pgdata_path" | awk '{print $1}')"
    echo_log "   - New size: $(du -hs "$new_pgdata_path" | awk '{print $1}')"

    if [ "$replace_pgdata" == true ]; then
      echo_log " - Replacing data"
      rm -rf "$pgdata_path"
      mv "$new_pgdata_path" "$pgdata_path"
    else
      echo_log " - Replacing data (SKIPPED)"
    fi

    rm -rf "$new_pgdata_path"
  fi
}

pre_checks
migrate_container "$container_path"
