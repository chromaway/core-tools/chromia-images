#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

source "$SCRIPT_DIR/commons.sh"

export NODE_URL=${NODE_URL:-https://system.chromaway.com:7740}
export SUBNODES_DIR=${SUBNODES_DIR:-/subnodes/}
export WORKING_DIR=${WORKING_DIR:-/tmp/pgdatamigration/}
export PG_SOURCE_VERSION=14
export PG_DESTINATION_VERSION=16
export MIGRATION_LOG="$WORKING_DIR/-vacuum.log"
export MY_UID=$(id -u)
export MY_GID=$(id -g)
export CONTAINER_DIRS=$(find "$SUBNODES_DIR" -name pgdata -type d | sed 's|/pgdata||g')
export CONTAINER_COUNT=$(echo "$CONTAINER_DIRS" | wc -w)
export verbose=${verbose:-false}
export replace_pgdata=${replace_pgdata:-false}
export backup_path=
export skip_dc_verification=false
vacuum_container=

function help() {
  echo "Usage: $0 [-v] [-r] [-b <backup-path>] [--skip-dc-verification] [--vacuum <container-name>]"
  echo "  -v, --verbose                 Verbose output"
  echo "  -r, --replace                 Replace the data in the container with the new version"
  echo "  -b, --backup <backup-path>    Backup path for the original data"
  echo "  --skip-dc-verification        Skip verification of the container in directory-chain"
  echo "  --vacuum <container-name>     Vacuum the container data by starting a postgres instance, running"
  echo "                                the vacuumdb command and stopping the postgres instance. Make sure"
  echo "                                no running Postgres instance is currently using this data directory."
}

while [[ $# -gt 0 ]]; do
  arg="$1"
  shift
  case $arg in
    -v|--verbose)
      verbose=true
      ;;
    -r|--replace)
      replace_pgdata=true
      ;;
    -b|--backup)
      backup_path="$1"
      shift
      ;;
    --skip-dc-verification)
      skip_dc_verification=true
      ;;
    --vacuum)
      vacuum_container="$1"
      shift
      ;;
    -h|--help)
      help
      exit 0
      ;;
    *)
      echo "Unknown parameter: $arg"
      help
      exit 1
      ;;
  esac
done

on_exit() {
  exit_code=$?
  if [ $exit_code -ne 0 ]; then
    echo "Error: view log for details: $MIGRATION_LOG"
    exit $exit_code
  fi
}

pre_checks
cd "$WORKING_DIR"
trap on_exit EXIT

# Vacuum container - exit when done
if [ -n "$vacuum_container" ]; then
  "$SCRIPT_DIR/container-vacuum.sh" "/subnodes/$vacuum_container"
  exit $?
fi

DC_BRID="$(curl -s "$NODE_URL/brid/iid_0")"
export DC_BRID
if [ -z "$DC_BRID" ]; then
  echo "Directory chain brid not found at $NODE_URL/brid/iid_0"
  exit 1
fi

current_counter=0
for container_path in $CONTAINER_DIRS; do

  echo_log
  echo_log
  echo_log "=> Migrating container $(( ++current_counter )) of $CONTAINER_COUNT: $(basename "$container_path")"
  echo_log

  "$SCRIPT_DIR/container-migrate.sh" "$container_path"

done

echo_log
echo_log "Migration of $current_counter containers completed successfully."
