#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

source "$SCRIPT_DIR/commons.sh"

container_path="$1"

function vacuum_pg {
  container_path="$1"
  container_path_name="$(basename "$container_path")"
  pgdata_path="$container_path/pgdata/"
  new_pgdata_path="$container_path/pgdata_v$PG_DESTINATION_VERSION"
  pg_version_path="$pgdata_path/PG_VERSION"
  container_name="$(echo $container_path_name | grep -Eo "[0-9A-F]{1,}-[^-]{1,}-[0-9]{1,}" | sed 's/^[^-]*-//g' | sed 's/-[^-]*$//g')"

  if [ ! -d "$pgdata_path" ] || [ ! -f "$pg_version_path" ]; then
    echo_log "Unexpected container data structure:"
    ls "$container_path"
    exit 1
  fi

  pg_version="$(cat "$pg_version_path")"
  if [ "$pg_version" == "$PG_DESTINATION_VERSION" ]; then
    echo_log "Container is already migrated to v$PG_DESTINATION_VERSION, skipping."
  elif [ "$pg_version" != "$PG_SOURCE_VERSION" ]; then
    echo_log "Unexpected postgres version in $pg_version_path, expected $PG_SOURCE_VERSION but got $pg_version"
    exit 1
  else

    if [ "$(find "$container_path" -exec stat -c "%u" {} \; | uniq)" != "$MY_UID" ]; then
      echo_log "Container files not fully owned by user $MY_UID"
      exit 1
    fi

    if [ "$(find "$container_path" -exec stat -c "%g" {} \; | uniq)" != "$MY_GID" ]; then
      echo_log "Container files not fully owned by group $MY_GID"
      exit 1
    fi

    if [ -f "$pgdata_path/postmaster.pid" ]; then
      echo_log "Pid file $pgdata_path/postmaster.pid found and removed"
      rm "$pgdata_path/postmaster.pid"
    fi

    echo_log " - Data size before: $(du -hs "$pgdata_path" | awk '{print $1}')"
    echo_log " - Starting Postgres v$PG_SOURCE_VERSION"
    "$PG_BIN_14/pg_ctl" -D "$pgdata_path" start >> "$MIGRATION_LOG" 2>&1
    echo_log " - Vacuuming data"
    verbose_output "$PG_BIN_14/vacuumdb" -U postgres --all --full
    echo_log " - Stopping Postgres v$PG_SOURCE_VERSION"
    verbose_output "$PG_BIN_14/pg_ctl" -D "$pgdata_path" stop
    echo_log " - Data size after: $(du -hs "$pgdata_path" | awk '{print $1}')"
  fi
}

pre_checks
vacuum_pg "$container_path"
